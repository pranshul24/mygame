# REACH OTHER END GAME

# CONTROLS :

- PLAYER 1:
    - UP KEY-for moving up
    - DOWN KEY-for moving down
    - LEFT KEY-for moving left
    - RIGHT KEY-for moving right

- PLAYER 2:
    - W KEY-for moving up
    - S KEY-for moving down
    - A KEY-for moving left
    - D KEY-for moving right
### NOTE: player 1 and player 2 play one after the other

# AIM:

- To reach the other end in least possible time to get higher score.

# SCORING:

1. There will be 3 stages between player 1 and player 2 and in every next stage speed increases.
2. Total score = sum of points in all 3 stages by the player
3. Total time = total time taken to complete all 3 stages by the player
2. The result will be based on (total score - 2*(time taken)) for all the 3 stages,the player 
    with greater value after all stages will win.
3. For dodging a moving obstacle in a row gives 10 points.
4. For dodging fixed obstacles in a row gives 5 points.
