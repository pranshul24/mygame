import pygame
import random
import time
import math
from config import *  # import data from config.py
pygame.init()
pygame.font.init()
pygame.mixer.init()
pygame.display.set_caption('Reach The Other End')
clock = pygame.time.Clock()
screen = pygame.display.set_mode((720, 705))  # gaming screen dimension
list_static = []
crash_sound = pygame.mixer.Sound("crash.wav")  # sound when crash happens
for h in range(1, 5):  # chosing random positions for fixed obstacles
    for m in range(1, 5):
        number = random.choice(range(low, high, 40))
        high += 180
        low += 180
        list_static.append(number)
    low = 1
    high = 120
r1 = random.randint(1, 500)
r2 = random.randint(1, 500)
r3 = random.randint(1, 500)
pygame.mixer.music.load('congo.mp3')  # including mp3 song
ant = pygame.image.load('ant.png')  # including the images that are being used
ant2 = pygame.image.load('ant2.png')
hen = pygame.image.load('hen.png')
snake = pygame.image.load('snake1.png')
snake_rev = pygame.image.load('snake2.png')


def score(count):  # display the score of current stage of the playing person
    font = pygame.font.Font(fontstyle, 45)
    text = font.render("Score: "+str(count), True, color)
    screen.blit(text, (0, 0))


def text_objects(text, font, color):
    textSurface = font.render(text, True, color)
    return textSurface, textSurface.get_rect()


def button(msg, x, y, w, h, ic, ac, action=None):  # input from button pressed
    mouse = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()
    if x+w > mouse[0] > x and y + h > mouse[1] > y:
        pygame.draw.rect(screen, ac, (x, y, w, h))
        if click[0] == 1 and action is not None:
            action()
    else:
        pygame.draw.rect(screen, ic, (x, y, w, h))  # creating box
    small_text = pygame.font.Font(fontstyle, 20)
    textSurf, textRect = text_objects(msg, small_text, color)
    textRect.center = ((x+(w/2)), (y+(h/2)))
    screen.blit(textSurf, textRect)


def move(cor, r):  # displaying the snake when going left to right
    screen.blit(snake, (cor, r))


def moveb(h, r):  # displaying the snake when going right to left
    screen.blit(snake_rev, (h, r))


def switch(obj):  # switching images(I have different IMG for snake)
    if obj.start <= 0:
        obj.flag = 1
    if obj.start >= 660:
        obj.flag = 2
    if obj.flag == 1:
        obj.start += speed
        move(obj.start, obj.level)
    if obj.flag == 2:
        obj.start -= speed
        moveb(obj.start, obj.level)


class moving:
    def __init__(self, level, start, flag):
        self.level = level
        self.start = random.choice(range(50, 650, 100))
        self.flag = random.randint(1, 2)


p1 = moving(200, 0, 1)  # creating object for moving class
p2 = moving(325, 0, 1)
p3 = moving(460, 0, 1)
p4 = moving(590, 0, 1)
p5 = moving(70, 0, 1)


def unpause():  # unpause screen
    global pause
    pause = False


def game_pause():  # pause screen
    screen.fill((180, 238, 180))
    largeText = pygame.font.Font(fontstyle, 90)
    TextSurf, TextRect = text_objects(mesg1, largeText, red)
    TextRect.center = (350, 360)
    screen.blit(TextSurf, TextRect)
    while pause:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:  # space for unpause
                    unpause()

        button("Continue", 150, 450, 100, 50, red,
               color2, unpause)  # create buttons
        button("QUIT", 450, 450, 100, 50, red, color2, pygame.quit)

        pygame.display.update()
        clock.tick(15)


def static_obj(lev):  # creating fixed obstacles and displaying them
    for o in range(0, 4):
        for g in range(0, 4):
            screen.blit(hen, (list_static[4*o+g]+1, lev))
        lev += 130


def crash():  # function for displaying when ant player crashes
    t_restart = time.time()+4
    pygame.mixer.Sound.play(crash_sound)  # crash sound
    screen.fill(color3)
    largeText = pygame.font.Font(fontstyle, 90)
    TextSurf, TextRect = text_objects(mesg2, largeText, red)
    TextRect.center = (360, 200)
    screen.blit(TextSurf, TextRect)
    largeText = pygame.font.Font(fontstyle, 40)
    if player == 1:
        TextSurf, TextRect = text_objects(mesg3+str(stage), largeText, red)
    else:
        TextSurf, TextRect = text_objects(mesg4+str(stage), largeText, red)
    TextRect.center = (360, 302)
    screen.blit(TextSurf, TextRect)

    if player == 1:
        TextSurf, TextRect = text_objects(
            mesg5+str(int(math.ceil(time_player1))), largeText, red)
    else:
        TextSurf, TextRect = text_objects(
            mesg5+str(int(math.ceil(time_player2))), largeText, red)
    TextRect.center = (360, 352)
    screen.blit(TextSurf, TextRect)

    if player == 1:
        TextSurf, TextRect = text_objects(
            mesg6+str(score_player1), largeText, red)
    else:
        TextSurf, TextRect = text_objects(
            mesg6+str(score_player2), largeText, red)
    TextRect.center = (360, 399)
    screen.blit(TextSurf, TextRect)
    while t_restart >= time.time():
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
        button("Quit", 310, 450, 100, 50, red, color1, pygame.quit)
        pygame.display.update()
        clock.tick(15)


pygame.display.flip()

list_move = [p1, p2, p3, p4, p5]


def game_round():  # increasing speed when level changes
    global speed
    speed += 3


def round():  # displaying when player completes a stage successfully
    t_restart = time.time()+4
    screen.fill(color3)
    largeText = pygame.font.Font(fontstyle, 80)
    TextSurf, TextRect = text_objects(mesg16, largeText, red)
    TextRect.center = (360, 190)
    screen.blit(TextSurf, TextRect)
    largeText = pygame.font.Font(fontstyle, 40)
    if player == 1:
        TextSurf, TextRect = text_objects(mesg3+str(stage), largeText, red)
    else:
        TextSurf, TextRect = text_objects(mesg4+str(stage), largeText, red)
    TextRect.center = (360, 302)
    screen.blit(TextSurf, TextRect)

    if player == 1:
        TextSurf, TextRect = text_objects(
            mesg5+str(int(math.ceil(time_player1))), largeText, red)
    else:
        TextSurf, TextRect = text_objects(
            mesg5+str(int(math.ceil(time_player2))), largeText, red)
    TextRect.center = (360, 352)
    screen.blit(TextSurf, TextRect)

    if player == 1:
        TextSurf, TextRect = text_objects(
            mesg6+str(score_player1), largeText, red)
    else:
        TextSurf, TextRect = text_objects(
            mesg6+str(score_player2), largeText, red)
    TextRect.center = (360, 399)
    screen.blit(TextSurf, TextRect)

    while t_restart >= time.time():
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
        pygame.display.update()
        clock.tick(15)


pygame.display.flip()


def result():  # for displaying the end result
    t_restart = time.time()+8
    pygame.mixer.music.play()  # for playing the song
    global score_player1
    global score_player2
    largeText = pygame.font.Font(fontstyle, 40)
    screen.fill(color3)  # for background color
    TextSurf, TextRect = text_objects(mesg3+str(stage), largeText, red)
    TextRect.center = (360, 162)
    screen.blit(TextSurf, TextRect)  # for displaying text
    mesg13 = str(int(math.ceil(time_player1)))
    TextSurf, TextRect = text_objects(
        mesg6+str(score_player1)+" " + mesg5+mesg13, largeText, red)
    TextRect.center = (360, 212)
    screen.blit(TextSurf, TextRect)
    TextSurf, TextRect = text_objects(mesg4+str(stage), largeText, red)
    TextRect.center = (360, 262)
    screen.blit(TextSurf, TextRect)
    mesg14 = str(int(math.ceil(time_player2)))
    TextSurf, TextRect = text_objects(
        mesg6+str(score_player2)+" "+mesg5+mesg14, largeText, red)
    TextRect.center = (360, 312)
    screen.blit(TextSurf, TextRect)
    score_player2 -= 2*int(math.ceil(time_player2))
    score_player1 -= 2*int(math.ceil(time_player1))
    TextSurf, TextRect = text_objects(mesg7+str(score_player1), largeText, red)
    TextRect.center = (360, 362)
    screen.blit(TextSurf, TextRect)
    TextSurf, TextRect = text_objects(mesg8+str(score_player2), largeText, red)
    TextRect.center = (360, 412)
    screen.blit(TextSurf, TextRect)
    largeText = pygame.font.Font(fontstyle, 65)
    if score_player1 > score_player2:
        TextSurf, TextRect = text_objects(mesg10, largeText, red)
    elif score_player1 < score_player2:
        TextSurf, TextRect = text_objects(mesg11, largeText, red)
    else:
        TextSurf, TextRect = text_objects(mesg12, largeText, red)
    TextRect.center = (360, 500)
    screen.blit(TextSurf, TextRect)
    while t_restart >= time.time():
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
        pygame.display.update()
        clock.tick(15)


i = 0

intro = True
t_end = time.time()+3
while intro and time.time() <= t_end:  # for displaying intro page
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()
    screen.fill(color3)
    largeText = pygame.font.Font(fontstyle, 60)
    TextSurf, TextRect = text_objects(mesg9, largeText, red)
    TextRect.center = (352, 300)
    screen.blit(TextSurf, TextRect)
    button("QUIT", 210, 450, 300, 50, red, color1, pygame.quit)  # quit button
    smallText = pygame.font.Font(fontstyle, 40)
    mesg15 = str(int(math.ceil((t_end-time.time()))))
    TextSurf, TextRect = text_objects(
        "Game starts in "+mesg15 + " sec", smallText, color1)
    TextRect.center = (352, 400)
    screen.blit(TextSurf, TextRect)
    pygame.display.update()
    clock.tick(15)

time_game = time.time()
while not game:  # main game loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            game = True
    pressed = pygame.key.get_pressed()
    if pressed[pygame.K_p]:  # P key for pause
        pause = True
        game_pause()
    elif pressed[pygame.K_UP] and y >= 3 and player == 1:  # keys for players
        y -= 3
    elif pressed[pygame.K_DOWN] and y <= 654 and player == 1:
        y += 3
    elif pressed[pygame.K_LEFT] and x > 0 and player == 1:
        x -= 3
    elif pressed[pygame.K_RIGHT] and x < 681 and player == 1:
        x += 3
    elif pressed[pygame.K_w] and y >= 3 and player == 2:
        y -= 3
    elif pressed[pygame.K_s] and y <= 654 and player == 2:
        y += 3
    elif pressed[pygame.K_a] and x > 0 and player == 2:
        x -= 3
    elif pressed[pygame.K_d] and x < 681 and player == 2:
        x += 3
    screen.fill(color4)
    if True:
        pygame.draw.rect(screen, color1, pygame.Rect(0, 0, 720, 58))
        pygame.draw.rect(screen, color1, pygame.Rect(0, 130, 720, 58))
        pygame.draw.rect(screen, color1, pygame.Rect(0, 260, 720, 58))
        pygame.draw.rect(screen, color1, pygame.Rect(0, 390, 720, 58))
        pygame.draw.rect(screen, color1, pygame.Rect(0, 520, 720, 58))
        pygame.draw.rect(screen, color1, pygame.Rect(0, 650, 720, 60))
    static_obj(130)
    for u in list_move:
        switch(u)
    if player == 1:
        screen.blit(ant, (x, y))
        largText = pygame.font.Font(fontstyle, 40)
        TextSurf, TextRect = text_objects("End", largText, red)
        TextRect.center = (360, 20)
        screen.blit(TextSurf, TextRect)
    if player == 2:
        screen.blit(ant2, (x, y))
        largText = pygame.font.Font(fontstyle, 40)
        TextSurf, TextRect = text_objects("End", largText, red)
        TextRect.center = (360, 674)
        screen.blit(TextSurf, TextRect)
    stat_lev = 130
    for i in range(0, 4):  # checking crash with fixed obstacles
        for j in range(0, 4):
            if y >= (stat_lev-ant_height) and y <= (stat_lev+hen_height):
                if x >= (list_static[4*i+j]-ant_width):
                    if x <= (list_static[4*i+j]+hen_width):
                        if player == 1:
                            score_player1 += points
                            value_time = math.ceil(time.time())
                            time_player1 += int(value_time-time_game)
                            crash()
                            time_game = time.time()
                            player = 2
                            x = 330
                            y = 3
                            y_max = 0
                        elif player == 2:
                            score_player2 += points2
                            value_time = math.ceil(time.time())
                            time_player2 += int(value_time-time_game)
                            crash()
                            time_game = time.time()
                            player = 1
                            x = 330
                            y = 654
                            y_max = 0
                            stage += 1
                            game_round()
                        points = 0
                        points2 = 0
                        y_min = 654
        stat_lev += 130
    for i in list_move:  # checking crash with moving obstacles
        if y >= (i.level-ant_height) and y <= (i.level+snake_height):
            if x >= (i.start-ant_width) and x <= (i.start+snake_width):
                if player == 1:
                    score_player1 += points
                    time_player1 += int(math.ceil(time.time()-time_game))
                    crash()
                    time_game = time.time()
                    player = 2
                    x = 330
                    y = 3
                    y_max = 0
                elif player == 2:
                    score_player2 += points2
                    time_player2 += int(math.ceil(time.time()-time_game))
                    crash()
                    time_game = time.time()
                    player = 1
                    x = 330
                    y = 654
                    y_max = 0
                    stage += 1
                    game_round()
                points = 0
                points2 = 0
                y_min = 654
    if stage == 4:
        result()
        pygame.quit()
    if y < y_min:
        y_min = y
    if y > y_max:
        y_max = y
    if player == 1:
        if y_min <= (58-ant_height):  # checking the score of current round
            points = 4*5+5*10
        elif y_min <= (130-ant_height):
            points = 4*5+4*10
        elif y_min <= (188-ant_height):
            points = 3*5+4*10
        elif y_min <= (260-ant_height):
            points = 3*5+3*10
        elif y_min <= (318-ant_height):
            points = 2*5+3*10
        elif y_min <= (390-ant_height):
            points = 2*5+2*10
        elif y_min <= (448-ant_height):
            points = 1*5+2*10
        elif y_min <= (520-ant_height):
            points = 1*5+1*10
        elif y_min <= (578-ant_height):
            points = 0*5+1*10
        else:
            points = 0*5+0*10
    if player == 2:
        if y_max < (130):
            points2 = 0
        elif y_max <= (188):
            points2 = 10
        elif y_max <= (260):
            points2 = 15
        elif y_max <= (318):
            points2 = 25
        elif y_max <= (390):
            points2 = 30
        elif y_max <= (448):
            points2 = 40
        elif y_max <= (520):
            points2 = 45
        elif y_max <= (578):
            points2 = 55
        elif y_max <= (650):
            points2 = 60
        else:
            points2 = 70
    if player == 1:  # resetting if round completed successfully by player 1
        score(points)
        if points == 70:
            time_player1 += int(math.ceil(time.time()-time_game))
            score_player1 += 70
            round()
            time_game = time.time()
            player = 2
            x = 330
            y = 3
            y_max = 0
            points = 0
            points2 = 0
            y_min = 654
    if player == 2:  # resetting if round completed successfully by player 2
        score(points2)
        if points2 == 70:
            time_player2 += int(math.ceil(time.time()-time_game))
            score_player2 += 70
            round()
            time_game = time.time()
            player = 1
            x = 330
            y = 654
            y_max = 0
            points2 = 0
            points = 0
            y_min = 654
            stage += 1
            game_round()
    if stage == 4:
        result()
        pygame.quit()
    pygame.display.flip()
    clock.tick(60)
